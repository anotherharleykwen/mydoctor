import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyAGEzsWw-GdgIJGkveCW2Y9KmBwuBJdOXk",
    authDomain: "my-doctor-01-56f3f.firebaseapp.com",
    databaseURL: "https://my-doctor-01-56f3f.firebaseio.com",
    projectId: "my-doctor-01-56f3f",
    storageBucket: "my-doctor-01-56f3f.appspot.com",
    messagingSenderId: "531489644079",
    appId: "1:531489644079:web:57d1e7ea127c1d80c1f0da"
  });

  const Fire = firebase;

  export default Fire;