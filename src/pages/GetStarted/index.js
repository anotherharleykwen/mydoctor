import React from 'react'
import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import { ILLogo, ILGetStarted } from '../../assets/illustration'
import { Button, Gap } from '../../components'
import { colors, fonts, getData } from '../../utils'

export default function GetStarted({navigation}) {

    const data = getData('user');
    return (
        <ImageBackground source={ILGetStarted} style={styles.page}>
            <View>
                <ILLogo />
                <Text style={styles.title}>Konsultasi dengan dokter jadi lebih mudah & fleksibel</Text>
            </View>
            <View>
                <Button title='Get Started' onPress={() => navigation.navigate('Register')} />
                <Gap height={16} />
                <Button type='secondary' title='Sign In' onPress={() => navigation.navigate('Login')} />
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    page: {padding: 40, backgroundColor: colors.white, flex: 1, justifyContent: "space-between"},
    title: {fontSize: 28, marginTop: 91, color: colors.white, fontFamily: fonts.primary[600]}
})
