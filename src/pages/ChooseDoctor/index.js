import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Header } from '../../components'

export default function ChooseDoctor({navigation}) {
    return (
        <View>
            <Header type='dark' title='Pilih Dokter Anak' onPress={() => navigation.goBack()}/>
            <Text>Choose Doctor</Text>
        </View>
    )
}

const styles = StyleSheet.create({})
