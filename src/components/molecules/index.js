import Header from './Header';
import BottomNavigator from './BottomNavigator';
import HomeProfile from './HomeProfile';
import DoctorCategory from './DoctorCategory';
import RatedDoctor from './RatedDoctor';
import NewsItem from './NewsItem';
import ListDoctor from './ListDoctor';
import Loading from './Loading';
import Profile from './Profile';
import List from './List';

export {
    Header, 
    BottomNavigator,
    HomeProfile,
    DoctorCategory,
    RatedDoctor,
    NewsItem,
    ListDoctor,
    Loading,
    Profile,
    List,
    };